'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _reactTimerMixin = require('react-timer-mixin');

var _reactTimerMixin2 = _interopRequireDefault(_reactTimerMixin);

var _reactMixin = require('react-mixin');

var _reactMixin2 = _interopRequireDefault(_reactMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var styles = _reactNative.StyleSheet.create({
  container: {
    flex: 1,
    overflow: 'hidden'
  },
  sceneContainerBase: {
    flex: 1,
    flexDirection: 'row'
  },
  paginationX: {
    position: 'absolute',
    bottom: 25,
    left: 0,
    right: 0,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  paginationY: {
    position: 'absolute',
    right: 15,
    top: 0,
    bottom: 0,
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  title: {
    height: 30,
    justifyContent: 'center',
    position: 'absolute',
    paddingLeft: 10,
    bottom: -30,
    left: 0,
    flexWrap: 'nowrap',
    width: 250,
    backgroundColor: 'transparent',

    borderColor: 'rgb(255,0,0)',
    borderWidth: 1
  },
  buttonWrapper: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    position: 'absolute',
    top: 0,
    left: 0,
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  buttonText: {
    fontSize: 50,
    color: '#007aff',
    fontFamily: 'Arial'
  },
  activeDot: {
    backgroundColor: '#007aff',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3
  },
  notActiveDot: {
    backgroundColor: 'rgba(0,0,0,.2)',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3
  }
});

var window = _reactNative.Dimensions.get('window');

var Swiper = function (_React$Component) {
  _inherits(Swiper, _React$Component);

  function Swiper(props) {
    _classCallCheck(this, Swiper);

    var _this = _possibleConstructorReturn(this, (Swiper.__proto__ || Object.getPrototypeOf(Swiper)).call(this, props));

    _this.props = props;

    var totalChildren = Array.isArray(props.children) ? props.children.length || 1 : 0;

    _this.state = {
      index: props.index,
      total: totalChildren,
      scrollValue: new _reactNative.Animated.Value(props.index),
      dir: props.horizontal === false ? 'y' : 'x',
      disableLeftNavigation: props.disableLeftNavigation,
      pageWidth: _this.props.pageWidth !== null ? _this.props.pageWidth : window.width,
      pageHeight: _this.props.pageHeight !== null ? _this.props.pageHeight : window.height,
      windowWidth: _this.props.windowWidth !== null ? _this.props.windowWidth : window.width,
      windowHeight: _this.props.windowHeight !== null ? _this.props.windowHeight : window.height
    };

    _this.onPanResponderMoveH = _this.onPanResponderMoveH.bind(_this);
    _this.onMoveShouldSetPanResponderH = _this.onMoveShouldSetPanResponderH.bind(_this);
    _this.onReleasePanResponderH = _this.onReleasePanResponderH.bind(_this);

    _this.onPanResponderMoveV = _this.onPanResponderMoveV.bind(_this);
    _this.onMoveShouldSetPanResponderV = _this.onMoveShouldSetPanResponderV.bind(_this);
    _this.onReleasePanResponderV = _this.onReleasePanResponderV.bind(_this);

    _this.vxThreshold = _reactNative.Platform.os === 'ios' ? 0.5 : 0.03;

    var offset = props.horizontal ? _this.getScrollPageOffsetH() : _this.getScrollPageOffsetV();

    _this.state.scrollValue.setOffset(offset);

    var prevScrollResponder = null;
    _this.state.scrollValue.addListener(function (scrollResponder) {
      if (prevScrollResponder === null || prevScrollResponder.value !== scrollResponder.value) {
        if (scrollResponder.value === _this.state.index) {
          _this.onScrollEnd(_this.state);
        }
      }
      prevScrollResponder = JSON.parse(JSON.stringify(scrollResponder));
    });

    if (props.horizontal) {
      _this.panResponder = _reactNative.PanResponder.create({
        onStartShouldSetPanResponder: function onStartShouldSetPanResponder() {
          return false;
        },
        onMoveShouldSetPanResponder: _this.onMoveShouldSetPanResponderH,
        onPanResponderRelease: _this.onReleasePanResponderH,
        onPanResponderTerminate: _this.onReleasePanResponderH,
        onPanResponderMove: _this.onPanResponderMoveH
      });
    } else {
      _this.panResponder = _reactNative.PanResponder.create({
        onMoveShouldSetPanResponder: _this.onMoveShouldSetPanResponderV.bind(_this),
        onPanResponderRelease: _this.onReleasePanResponderV.bind(_this),
        onPanResponderTerminate: _this.onPanResponderTerminateV.bind(_this),
        onPanResponderMove: _this.onPanResponderMoveV.bind(_this)
      });
    }
    return _this;
  }

  _createClass(Swiper, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.autoplay();
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps, nextState) {
      var _this2 = this;

      var totalChildren = Array.isArray(nextProps.children) ? nextProps.children.length || 1 : 0;
      this.setState({ index: nextProps.index, total: totalChildren, disableLeftNavigation: nextProps.disableLeftNavigation }, function () {
        if (_this2.props.index !== nextProps.index && nextProps.index !== _this2.state.index) {
          _this2.scrollTo(nextProps.index, false);
        }
      });
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.state.scrollValue.removeAllListeners();
    }
  }, {
    key: 'onReleasePanResponderH',
    value: function onReleasePanResponderH(e, gestureState) {
      var relativeGestureDistance = gestureState.dx / this.state.windowWidth;
      var vx = gestureState.vx;


      var newIndex = this.updateIndex(this.state.index, vx, relativeGestureDistance);
      this.scrollTo(newIndex, false);
    }
  }, {
    key: 'onReleasePanResponderV',
    value: function onReleasePanResponderV(e, gestureState) {
      var relativeGestureDistance = gestureState.dy / this.state.windowHeight;
      var vy = gestureState.vy;


      var newIndex = this.updateIndex(this.state.index, vy, relativeGestureDistance);

      this.scrollTo(newIndex, false);
    }
  }, {
    key: 'onPanResponderTerminateV',
    value: function onPanResponderTerminateV(e, gestureState) {
      var relativeGestureDistance = gestureState.dy / this.state.windowHeight;
      var vy = gestureState.vy;


      var newIndex = this.updateIndex(this.state.index, vy, relativeGestureDistance);

      this.scrollTo(newIndex, false);
    }
  }, {
    key: 'onMoveShouldSetPanResponderH',
    value: function onMoveShouldSetPanResponderH(e, gestureState) {
      var _props = this.props,
          threshold = _props.threshold,
          scrollEnabled = _props.scrollEnabled,
          responderTaken = _props.responderTaken;


      if (!scrollEnabled || responderTaken()) {
        return false;
      }

      if (Math.abs(gestureState.dx) > Math.abs(gestureState.dy) && Math.abs(gestureState.dx) > 60) {
        if (gestureState.dx < -0.4 + -this.props.index && !this.props.disableLeftSwipe || gestureState.dx > 0.4 + this.props.index && !this.props.disableRightSwipe) {
          this.props.onScrollBeginDrag();
          return true;
        }
      }

      return false;
    }
  }, {
    key: 'onMoveShouldSetPanResponderV',
    value: function onMoveShouldSetPanResponderV(e, gestureState) {
      var _props2 = this.props,
          threshold = _props2.threshold,
          scrollEnabled = _props2.scrollEnabled,
          responderTaken = _props2.responderTaken;


      if (!scrollEnabled || responderTaken()) {
        return false;
      }

      if (threshold - Math.abs(gestureState.dy) > 0) {
        return false;
      }

      if (Math.abs(gestureState.dy) > Math.abs(gestureState.dx) && Math.abs(gestureState.dy) > 60) {
        this.props.onScrollBeginDrag();
        return true;
      }

      return false;
    }
  }, {
    key: 'onPanResponderMoveH',
    value: function onPanResponderMoveH(e, gestureState) {
      var dx = gestureState.dx;
      var offsetX = -dx / this.state.pageWidth + this.state.index;
      if (gestureState.dx < 0 && this.props.disableLeftSwipe !== true || gestureState.dx > 0 && this.props.disableRightSwipe !== true) {
        if (offsetX >= 0 && offsetX < this.props.children.length - 1) {
          this.state.scrollValue.setValue(offsetX);
        }
      }
    }
  }, {
    key: 'onPanResponderMoveV',
    value: function onPanResponderMoveV(e, gestureState) {
      var dy = gestureState.dy;
      var offsetY = -dy / this.state.pageHeight + this.state.index;

      if (offsetY >= 0 && offsetY < this.props.children.length - 1) {
        this.state.scrollValue.setValue(offsetY);
      }
    }
  }, {
    key: 'onScrollEnd',
    value: function onScrollEnd(status) {
      this.props.onMomentumScrollEnd(null, status, this);
    }
  }, {
    key: 'onTouchEnd',
    value: function onTouchEnd(status) {
      var _this3 = this;

      this.props.onMomentumTouchEnd(null, status, this);
      setTimeout(function () {
        _this3.autoplay();
      });
    }
  }, {
    key: 'getScrollPageOffsetH',
    value: function getScrollPageOffsetH() {
      if (this.state.pageWidth === this.state.windowWidth) {
        return 0;
      }
      var offsetWindowRatio = (this.state.windowWidth - this.state.pageWidth) / (this.state.windowWidth / 100) / 2 / 100;
      var scaleToPageRatio = this.state.windowWidth / this.state.pageWidth;

      return -offsetWindowRatio * scaleToPageRatio;
    }
  }, {
    key: 'getScrollPageOffsetV',
    value: function getScrollPageOffsetV() {
      if (this.state.pageHeight === this.state.windowHeight) {
        return 0;
      }
      var offsetWindowRatio = (this.state.windowHeight - this.state.pageHeight) / (this.state.windowHeight / 100) / 2 / 100;
      var scaleToPageRatio = this.state.windowHeight / this.state.pageHeight;

      return -offsetWindowRatio * scaleToPageRatio;
    }
  }, {
    key: 'updateIndex',
    value: function updateIndex(index, vx, relativeGestureDistance) {
      var distanceThreshold = 0.5;

      if (relativeGestureDistance < -distanceThreshold || relativeGestureDistance < 0 && vx <= -this.vxThreshold) {
        if (!this.props.disableLeftSwipe) {
          if (!this.shouldDisableLeftNavigation(index)) {
            return index + 1;
          } else if (this.props.shakeSwipedDisabledNavigation) {
            // TODO: callback for spring animation
          }
        }
      }

      if (relativeGestureDistance > distanceThreshold || relativeGestureDistance > 0 && vx >= this.vxThreshold) {
        if (!this.props.disableRightSwipe) {
          if (!this.props.disableRightNavigation) {
            return index - 1;
          } else if (this.props.shakeSwipedDisabledNavigation) {
            // TODO: callback for spring animation
          }
        }
      }
      return index;
    }
  }, {
    key: 'scrollTo',
    value: function scrollTo(pageNumber, forceScroll) {
      var _this4 = this;

      var isWeb = _reactNative.Platform.OS === 'web';
      // const newPageNumber = Math.max(0, Math.min(pageNumber, this.props.children.length - 1));
      if (this.props.loop || pageNumber >= 0 && pageNumber < this.state.total || forceScroll) {
        var newPageNumber = pageNumber >= 0 ? pageNumber % this.state.total : this.props.children.length - 1;
        var oldPageNumber = this.state.index;

        var leftScrollNotAllowed = !forceScroll && this.shouldDisableLeftNavigation(oldPageNumber) && oldPageNumber < newPageNumber;
        var rightScrollNotAllowed = !forceScroll && this.props.disableRightNavigation && oldPageNumber < newPageNumber;

        this.setState({ index: newPageNumber }, function () {
          if (leftScrollNotAllowed || rightScrollNotAllowed) {
            if (isWeb) {
              _this4.scrollTo(oldPageNumber, true);
            } else {
              setTimeout(function () {
                _this4.scrollTo(oldPageNumber, true);
              }, _this4.props.scrollDurationMs / 2);
            }
          }
        });

        _reactNative.Animated.timing(this.state.scrollValue, { toValue: newPageNumber, duration: this.props.scrollDurationMs }).start();

        var status = Object.assign({}, this.state, { index: newPageNumber });

        this.onTouchEnd(status);
      }
    }
  }, {
    key: 'scrollBy',
    value: function scrollBy(indexOffset) {
      this.scrollTo(this.state.index + indexOffset, false);
    }
  }, {
    key: 'autoplay',
    value: function autoplay() {
      var _this5 = this;

      if (!Array.isArray(this.props.children) || !this.props.autoplay) {
        return;
      }

      clearTimeout(this.autoplayTimer);

      this.autoplayTimer = setTimeout(function () {
        _this5.scrollBy(_this5.props.autoplayDirection ? 1 : -1);
      }, this.props.autoplayTimeout * 1000);
    }
  }, {
    key: 'renderDotPagination',
    value: function renderDotPagination() {
      // By default, dots only show when `total` > 2
      if (this.state.total <= 1) {
        return null;
      }

      var dots = [];
      var ActiveDot = this.props.activeDot || _react2.default.createElement(_reactNative.View, { style: styles.activeDot });
      var Dot = this.props.dot || _react2.default.createElement(_reactNative.View, { style: styles.notActiveDot });

      for (var i = 0; i < this.state.total; i++) {
        dots.push(i === this.state.index ? _react2.default.cloneElement(ActiveDot, { key: i }) : _react2.default.cloneElement(Dot, { key: i }));
      }

      return _react2.default.createElement(
        _reactNative.View,
        {
          pointerEvents: 'none',
          style: [styles['pagination' + this.state.dir.toUpperCase()], this.props.paginationStyle]
        },
        dots
      );
    }
  }, {
    key: 'shouldDisableLeftNavigation',
    value: function shouldDisableLeftNavigation(index) {
      if (typeof this.state.disableLeftNavigation === 'boolean') {
        return this.state.disableLeftNavigation;
      }
      if (this.state.disableLeftNavigation && Array.isArray(this.state.disableLeftNavigation)) {
        if (this.state.disableLeftNavigation[index]) {
          return this.state.disableLeftNavigation[index];
        }
        return false;
      }
      return false;
    }
  }, {
    key: 'renderPagination',
    value: function renderPagination() {
      if (!this.props.showsPagination) {
        return null;
      }

      if (this.props.renderPagination) {
        return this.props.renderPagination(this.state.index, this.props.children.length);
      }
      return this.renderDotPagination();
    }
  }, {
    key: 'renderTitle',
    value: function renderTitle() {
      var child = this.props.children[this.state.index];
      var title = child && child.props && child.props.title;

      return title ? _react2.default.createElement(
        _reactNative.View,
        { style: styles.title },
        this.props.children[this.state.index].props.title
      ) : null;
    }
  }, {
    key: 'renderNextButton',
    value: function renderNextButton() {
      var button = null;

      if (this.props.loop || this.state.index !== this.state.total - 1 && !this.props.disableLeftSwipe) {
        button = this.props.nextButton || _react2.default.createElement(
          _reactNative.Text,
          { style: styles.buttonText },
          '\u203A'
        );
      }

      return _react2.default.createElement(
        _reactNative.TouchableOpacity,
        { onPress: this.props.nextButtonCb !== null ? this.props.nextButtonCb.bind(this) : this.scrollBy.bind(this, 1) },
        _react2.default.createElement(
          _reactNative.View,
          null,
          button
        )
      );
    }
  }, {
    key: 'renderPrevButton',
    value: function renderPrevButton() {
      var button = null;

      if (this.props.loop || this.state.index !== 0 && !this.props.disableRightSwipe) {
        button = this.props.prevButton || _react2.default.createElement(
          _reactNative.Text,
          { style: styles.buttonText },
          '\u2039'
        );
      }

      return _react2.default.createElement(
        _reactNative.TouchableOpacity,
        { onPress: this.props.prevButtonCb !== null ? this.props.prevButtonCb.bind(this) : this.scrollBy.bind(this, -1) },
        _react2.default.createElement(
          _reactNative.View,
          null,
          button
        )
      );
    }
  }, {
    key: 'renderButtons',
    value: function renderButtons() {
      return _react2.default.createElement(
        _reactNative.View,
        {
          pointerEvents: 'box-none',
          style: [styles.buttonWrapper, { width: this.state.pageWidth, height: this.state.pageHeight }, this.props.buttonWrapperStyle]
        },
        this.renderPrevButton(),
        this.renderNextButton()
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this6 = this;

      var pages = this.props.children.map(function (page, index) {
        var pageStyle = {
          width: _this6.state.pageWidth,
          height: _this6.state.pageHeight,
          backgroundColor: 'transparent'
        };
        if (_reactNative.Platform.OS === 'web') {
          pageStyle.display = _this6.state.index === index ? 'flex' : 'none';
          pageStyle.overflow = 'scroll';
        }
        return _react2.default.createElement(
          _reactNative.View,
          { style: pageStyle, key: '' + index },
          page
        );
      });

      var translateX = this.state.scrollValue.interpolate({
        inputRange: [0, 1], outputRange: [0, -this.state.pageWidth]
      });

      var translateY = this.state.scrollValue.interpolate({
        inputRange: [0, 1], outputRange: [0, -this.state.pageHeight]
      });

      var transform = this.props.horizontal ? { transform: [{ translateX: translateX }] } : { transform: [{ translateY: translateY }] };

      var sceneContainerStyle = {
        flexDirection: this.props.horizontal ? 'row' : 'column',
        width: this.props.horizontal ? this.state.pageWidth * this.props.children.length : null,
        height: this.props.horizontal ? null : this.state.pageHeight * this.props.children.length
      };

      if (_reactNative.Platform.OS !== 'web') {
        return _react2.default.createElement(
          _reactNative.View,
          {
            style: styles.container
          },
          _react2.default.createElement(
            _reactNative.Animated.View,
            _extends({}, this.panResponder.panHandlers, {
              style: [sceneContainerStyle, transform]
            }),
            pages
          ),
          this.props.showsPagination && this.renderPagination(),
          this.renderTitle(),
          this.props.showsButtons && this.renderButtons()
        );
      }
      return _react2.default.createElement(
        _reactNative.View,
        {
          style: styles.container
        },
        _react2.default.createElement(
          _reactNative.View,
          { style: sceneContainerStyle },
          pages
        ),
        this.props.showsPagination && this.renderPagination(),
        this.renderTitle(),
        this.props.showsButtons && this.renderButtons()
      );
    }
  }]);

  return Swiper;
}(_react2.default.Component);

Swiper.propTypes = {
  activeDot: _react2.default.PropTypes.element,
  autoplay: _react2.default.PropTypes.bool,
  autoplayDirection: _react2.default.PropTypes.bool,
  autoplayTimeout: _react2.default.PropTypes.number,
  buttonWrapperStyle: _react2.default.PropTypes.object,
  children: _react2.default.PropTypes.node.isRequired,
  disableLeftNavigation: _react2.default.PropTypes.any,
  disableRightNavigation: _react2.default.PropTypes.bool,
  disableLeftSwipe: _react2.default.PropTypes.bool,
  disableRightSwipe: _react2.default.PropTypes.bool,
  dot: _react2.default.PropTypes.element,
  horizontal: _react2.default.PropTypes.bool,
  index: _react2.default.PropTypes.number,
  loop: _react2.default.PropTypes.bool,
  nextButton: _react2.default.PropTypes.element,
  nextButtonCb: _react2.default.PropTypes.func,
  onMomentumScrollEnd: _react2.default.PropTypes.func,
  onMomentumTouchEnd: _react2.default.PropTypes.func,
  onScrollBeginDrag: _react2.default.PropTypes.func,
  pageHeight: _react2.default.PropTypes.number,
  pageWidth: _react2.default.PropTypes.number,
  paginationStyle: _react2.default.PropTypes.object,
  prevButton: _react2.default.PropTypes.element,
  prevButtonCb: _react2.default.PropTypes.func,
  renderPagination: _react2.default.PropTypes.func,
  responderTaken: _react2.default.PropTypes.func,
  scrollDurationMs: _react2.default.PropTypes.number,
  scrollEnabled: _react2.default.PropTypes.bool,
  shakeSwipedDisabledNavigation: _react2.default.PropTypes.bool,
  showsButtons: _react2.default.PropTypes.bool,
  showsPagination: _react2.default.PropTypes.bool,
  threshold: _react2.default.PropTypes.number,
  windowHeight: _react2.default.PropTypes.number,
  windowWidth: _react2.default.PropTypes.number
};

Swiper.defaultProps = {
  disableLeftSwipe: false,
  disableRightSwipe: false,
  disableLeftNavigation: false,
  disableRightNavigation: false,
  shakeSwipedDisabledNavigation: false,
  index: 0,
  threshold: 65,
  onMomentumScrollEnd: function onMomentumScrollEnd() {},
  onMomentumTouchEnd: function onMomentumTouchEnd() {},
  scrollDurationMs: 250,
  renderPagination: null,
  onScrollBeginDrag: function onScrollBeginDrag() {},
  scrollEnabled: true,
  responderTaken: function responderTaken() {
    return false;
  },
  pageWidth: window.width,
  pageHeight: window.height,
  horizontal: true,
  loop: true,
  autoplay: true,
  autoplayDirection: true,
  autoplayTimeout: 2.5,
  buttonWrapperStyle: {},
  prevButton: null,
  prevButtonCb: null,
  nextButton: null,
  nextButtonCb: null,
  showsButtons: true,
  showsPagination: false,
  windowHeight: window.height,
  windowWidth: window.width
};

_reactMixin2.default.onClass(Swiper, _reactTimerMixin2.default);

module.exports = Swiper;